package com.booking.alore.model;

import com.booking.alore.entity.Hotel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class RoomDto implements Serializable {
    private Long roomId;
    private String type;
    private int cost;
    private Hotel hotel;
}
