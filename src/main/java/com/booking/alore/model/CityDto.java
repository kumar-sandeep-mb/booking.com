package com.booking.alore.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class CityDto implements Serializable {
    private Long cityId;
    private String cityName;
}
