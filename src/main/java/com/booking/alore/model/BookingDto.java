package com.booking.alore.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.sql.Date;

@Getter
@Setter
@ToString
public class BookingDto implements Serializable {
    private Long id;
    private UserDto user;
    private RoomDto room;
    private Date checkInDate;
    private Date checkOutDate;
    private int guests;
    private boolean cancelled;
    private boolean completed;
}
