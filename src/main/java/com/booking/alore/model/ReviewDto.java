package com.booking.alore.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class ReviewDto implements Serializable {
    private Long reviewId;
    private UserDto user;
    private HotelDto hotel;
    private String comment;
    private int rating;
}
