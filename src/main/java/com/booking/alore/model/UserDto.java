package com.booking.alore.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@ToString
public class UserDto implements Serializable {
    private Long userId;
    @NotBlank(message = "username is mandatory")
    private String username;
    @Transient
    @NotBlank(message = "password is mandatory")
    private String password;
    private String name;
    private String mobile;
    private String gender;
    private String city;
}
