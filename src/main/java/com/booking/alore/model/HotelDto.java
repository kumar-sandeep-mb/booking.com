package com.booking.alore.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class HotelDto implements Serializable {
    private Long hotelId;
    private String hotelName;
    private int star;
    private boolean wifiAvailable;
    private boolean restaurantAvailable;
    private boolean airConditioning;
    private boolean mealIncluded;
    private String address;
    private CityDto city;
    private float rating;
    private int minCost;
}
