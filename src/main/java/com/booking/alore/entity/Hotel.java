package com.booking.alore.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity(name = "hotel_table")
public class Hotel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long hotelId;

    @Column(unique = true)
    private String hotelName;

    @Column
    private int star;

    @Column
    private boolean wifiAvailable;

    @Column
    private boolean restaurantAvailable;

    @Column
    private boolean airConditioning;

    @Column
    private boolean mealIncluded;

    @Column
    private String address;

    @ManyToOne
    @JoinColumn(nullable = false, name = "city_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private City city;

    @Column
    private float rating;

    @Column
    private int minCost;
}
