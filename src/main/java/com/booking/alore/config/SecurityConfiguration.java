package com.booking.alore.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

//TODO : enable user authentication
@Configuration
@EnableWebSecurity
@Slf4j
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    //TODO : enable security for all endpoints
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        boolean securityEnabled = false;
        if (!securityEnabled) {
            log.info("Security is disabled");
            http.csrf().disable().authorizeRequests().antMatchers("/**").permitAll();
        } else {
            log.info("Security is enabled");
            http.csrf().disable().cors().disable().authorizeRequests()
                    .antMatchers(HttpMethod.POST, "/book-room").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.POST, "/booking/{bookingId}/cancel-booking").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.POST, "/booking/{bookingId}/complete-booking").hasAnyAuthority("writer")
                    .antMatchers(HttpMethod.GET, "/hotels").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.GET, "/hotels/available").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.GET, "/hotel/{hotelId}").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.POST, "/hotel").hasAnyAuthority("admin")
                    .antMatchers(HttpMethod.PUT, "/hotel").hasAnyAuthority("admin")
                    .antMatchers(HttpMethod.DELETE, "/hotel/{hotelId}").hasAnyAuthority("admin")
                    .antMatchers(HttpMethod.POST, "/hotel/review").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.DELETE, "/hotel/review/{reviewId}").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.POST, "/room").hasAnyAuthority("admin")
                    .antMatchers(HttpMethod.DELETE, "/room/{roomId}").hasAnyAuthority("admin")
                    .antMatchers(HttpMethod.GET, "/users").hasAnyAuthority("admin")
                    .antMatchers(HttpMethod.GET, "/user/{userId}").hasAnyAuthority("admin")
                    .antMatchers(HttpMethod.POST, "/user").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.PUT, "/user").hasAnyAuthority("user")
                    .antMatchers(HttpMethod.DELETE, "/user/{userId}").hasAnyAuthority("user")
                    .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }
    }

    /* Password encoding bean */
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }
}