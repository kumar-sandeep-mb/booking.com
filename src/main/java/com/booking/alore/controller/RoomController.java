package com.booking.alore.controller;

import com.booking.alore.model.RoomDto;
import com.booking.alore.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//TODO : validate user input
@RestController
public class RoomController {
    @Autowired
    private RoomService roomService;

    @PostMapping("/room")
    public ResponseEntity<?> saveRoom(@RequestBody RoomDto roomDto) {
        return roomService.saveRoom(roomDto);
    }

    @DeleteMapping("/room/{roomId}")
    public ResponseEntity<?> saveRoom(@PathVariable String roomId) {
        return roomService.deleteRoom(Long.parseLong(roomId));
    }
}
