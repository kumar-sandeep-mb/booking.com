package com.booking.alore.controller;

import com.booking.alore.model.HotelDto;
import com.booking.alore.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

//TODO : validate user input
@RestController
public class HotelController {
    @Autowired
    private HotelService hotelService;

    @GetMapping("/hotels")
    public ResponseEntity<?> getHotels(@RequestParam(required = false) String city, @RequestParam int star,
                                       @RequestParam(required = false) boolean wifiAvailability,
                                       @RequestParam(required = false) boolean restaurantAvailability,
                                       @RequestParam(required = false) boolean acAvailability,
                                       @RequestParam(required = false) boolean mealsIncluded,
                                       @RequestParam(required = false) String sortBy) {
        return hotelService.getHotels(city, star, wifiAvailability,
                restaurantAvailability, acAvailability, mealsIncluded, sortBy);
    }

    @GetMapping("/hotels/available")
    public ResponseEntity<?> getAvailableHotels(@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date checkInDate,
                                                @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date checkoutDate,
                                                @RequestParam(required = false) long roomsRequired) {
        return hotelService.getAvailableHotels(checkInDate, checkoutDate, roomsRequired);
    }

    @GetMapping("/hotel/{hotelId}")
    public ResponseEntity<?> getHotel(@PathVariable String hotelId) {
        return hotelService.getHotel(Long.parseLong(hotelId));
    }

    @PostMapping("/hotel")
    public ResponseEntity<?> saveHotel(@RequestBody HotelDto hotelDto) {
        return hotelService.saveHotel(hotelDto);
    }

    @PutMapping("/hotel")
    public ResponseEntity<?> updateHotel(@RequestBody HotelDto hotelDto) {
        return hotelService.updateHotel(hotelDto);
    }

    @DeleteMapping("/hotel/{hotelId}")
    public ResponseEntity<?> deleteHotel(@PathVariable String hotelId) {
        return hotelService.deleteHotel(Long.parseLong(hotelId));
    }
}
