package com.booking.alore.controller;

import com.booking.alore.model.ReviewDto;
import com.booking.alore.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//TODO : validate user input
@RestController
public class ReviewController {
    @Autowired
    private ReviewService reviewService;

    @PostMapping("/hotel/review")
    public ResponseEntity<?> saveReview(@RequestBody ReviewDto reviewDto) {
        return reviewService.saveReview(reviewDto);
    }

    @DeleteMapping("/hotel/review/{reviewId}")
    public ResponseEntity<?> deleteReview(@PathVariable String reviewId) {
        return reviewService.deleteReview(Long.parseLong(reviewId));
    }
}
