package com.booking.alore.controller;

import com.booking.alore.model.BookingDto;
import com.booking.alore.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//TODO : validate user input
@RestController
public class BookingController {
    @Autowired
    private BookingService bookingService;

    @PostMapping("/book-room")
    public ResponseEntity<?> bookRoom(@RequestBody BookingDto bookingDto) {
        return bookingService.bookRoom(bookingDto);
    }

    @PostMapping("/booking/{bookingId}/cancel-booking")
    public ResponseEntity<?> cancelBooking(@PathVariable String bookingId) {
        return bookingService.cancelBooking(Long.parseLong(bookingId));
    }

    @PostMapping("/booking/{bookingId}/complete-booking")
    public ResponseEntity<?> completeBooking(@PathVariable String bookingId) {
        return bookingService.completeBooking(Long.parseLong(bookingId));
    }
}
