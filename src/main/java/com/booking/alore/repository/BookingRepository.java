package com.booking.alore.repository;

import com.booking.alore.entity.Booking;
import com.booking.alore.entity.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {

    /* Query to select the hotels having availability of the required rooms within selected date range (checkin - checkout) */
    @Query("SELECT h from hotel_table h where h.hotelId in (SELECT r.hotel.hotelId from room_table r where (r.roomId not in (SELECT b.room.roomId from booking_table b where ((?1 <= b.checkInDate and ?2 >= b.checkInDate) or (?1 <= b.checkOutDate and ?2 >= b.checkOutDate) or (b.checkInDate <= ?1 and b.checkOutDate >= ?2)) and b.cancelled = false)) group by r.hotel.hotelId having count(r.hotel.hotelId) >= ?3)")
    List<Hotel> findAvailableHotels(Date checkInDate, Date checkOutDate, long roomRequired);
}
