package com.booking.alore.repository;

import com.booking.alore.entity.Hotel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

//TODO : remove hardcoded strings
public class HotelRepositoryImpl implements CustomHotelRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Hotel> findHotels(long cityId, int star, boolean wifiAvailability, boolean restaurantAvailability,
                                  boolean acAvailability, boolean mealsIncluded, String sortBy) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Hotel> query = cb.createQuery(Hotel.class);
        Root<Hotel> hotel = query.from(Hotel.class);

        Path<String> hotelCity = hotel.get("city");
        Path<Integer> hotelStar = hotel.get("star");
        Path<Boolean> hotelWifiAvailability = hotel.get("wifiAvailable");
        Path<Boolean> hotelRestaurantAvailability = hotel.get("restaurantAvailable");
        Path<Boolean> hotelAcAvailability = hotel.get("airConditioning");
        Path<Boolean> hotelMealsIncluded = hotel.get("mealIncluded");
        Path<Integer> hotelCost = hotel.get("minCost");
        Path<Float> hotelRating = hotel.get("rating");

        List<Predicate> predicates = new ArrayList<>();
        if (cityId > 0) {
            predicates.add(cb.equal(hotelCity, cityId));
        }
        predicates.add(cb.greaterThanOrEqualTo(hotelStar, star));
        if (wifiAvailability) {
            predicates.add(cb.equal(hotelWifiAvailability, wifiAvailability));
        }
        if (restaurantAvailability) {
            predicates.add(cb.equal(hotelRestaurantAvailability, restaurantAvailability));
        }
        if (acAvailability) {
            predicates.add(cb.equal(hotelAcAvailability, acAvailability));
        }
        if (mealsIncluded) {
            predicates.add(cb.equal(hotelMealsIncluded, mealsIncluded));
        }

        /* sorting logic based on cost or rating*/
        if (sortBy != null) {
            if (sortBy.equals("cost")) {
                query.orderBy(cb.asc(hotelCost));
            }
            if (sortBy.equals("rating")) {
                query.orderBy(cb.desc(hotelRating));
            }
        }

        /* selection logic */
        query.select(hotel)
                .where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        return entityManager.createQuery(query)
                .getResultList();
    }
}
