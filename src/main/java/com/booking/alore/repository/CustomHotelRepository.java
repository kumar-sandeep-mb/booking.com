package com.booking.alore.repository;

import com.booking.alore.entity.Hotel;

import java.util.List;

public interface CustomHotelRepository {
    List<Hotel> findHotels(long cityId, int star, boolean wifiAvailability, boolean restaurantAvailability,
                           boolean acAvailability, boolean mealsIncluded, String sortBy);
}
