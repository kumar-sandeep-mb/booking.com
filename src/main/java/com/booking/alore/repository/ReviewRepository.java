package com.booking.alore.repository;

import com.booking.alore.entity.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

    /* query to return average rating of the hotel based on all users rating */
    @Query("SELECT AVG(r.rating) FROM review_table r WHERE r.hotel.hotelId = ?1")
    float findAverageRatingOfHotel(Long hotelId);
}
