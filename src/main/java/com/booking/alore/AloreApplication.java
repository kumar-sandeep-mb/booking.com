package com.booking.alore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//TODO : write readme file on how to start and use the application
//TODO : write unit test cases
//TODO : write db scripts(optional). Tables will be automatically created by spring
@SpringBootApplication
public class AloreApplication {

    public static void main(String[] args) {
        SpringApplication.run(AloreApplication.class, args);
    }

}
