package com.booking.alore.service;

import com.booking.alore.entity.Booking;
import com.booking.alore.model.BookingDto;
import com.booking.alore.repository.BookingRepository;
import com.booking.alore.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Date;
import java.util.Optional;

//TODO : handle exceptions
//TODO : put logs
@Service
public class BookingService {
    @Autowired
    private BookingRepository bookingRepository;

    public ResponseEntity<?> bookRoom(BookingDto bookingDto) {
        Date currentDate = new Date();
        if (currentDate.before(bookingDto.getCheckInDate()) || currentDate.equals(bookingDto.getCheckInDate())) {
            Booking booking = ObjectMapperUtils.map(bookingDto, Booking.class);
            bookingRepository.save(booking);
            return ResponseEntity.noContent().build();
        }
        throw new HttpClientErrorException(HttpStatus.NOT_ACCEPTABLE);
    }

    public ResponseEntity<?> cancelBooking(Long bookingId) {
        Optional<Booking> booking = bookingRepository.findById(bookingId);
        if (booking.isPresent()) {
            Booking bookingToBeCancelled = booking.get();
            if (bookingToBeCancelled.isCancelled()) {
                throw new HttpClientErrorException(HttpStatus.CONFLICT);
            }
            Date currentDate = new Date();
            if (currentDate.before(bookingToBeCancelled.getCheckInDate())) {
                bookingToBeCancelled.setCancelled(true);
                bookingRepository.save(bookingToBeCancelled);
                return ResponseEntity.noContent().build();
            }
            throw new HttpClientErrorException(HttpStatus.NOT_ACCEPTABLE);
        }
        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> completeBooking(Long bookingId) {
        Optional<Booking> booking = bookingRepository.findById(bookingId);
        if (booking.isPresent()) {
            Booking bookingToBeCompleted = booking.get();
            if (bookingToBeCompleted.isCompleted()) {
                throw new HttpClientErrorException(HttpStatus.CONFLICT);
            }
            Date currentDate = new Date();
            if (currentDate.after(bookingToBeCompleted.getCheckInDate()) ||
                    currentDate.equals(bookingToBeCompleted.getCheckInDate())) {
                bookingToBeCompleted.setCompleted(true);
                bookingRepository.save(bookingToBeCompleted);
                return ResponseEntity.noContent().build();
            }
            throw new HttpClientErrorException(HttpStatus.NOT_ACCEPTABLE);
        }
        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
    }
}
