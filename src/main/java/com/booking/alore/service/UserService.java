package com.booking.alore.service;

import com.booking.alore.entity.User;
import com.booking.alore.model.UserDto;
import com.booking.alore.repository.UserRepository;
import com.booking.alore.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;

    public ResponseEntity<List<UserDto>> getUsers() {
        List<User> users = userRepository.findAll();
        if (!users.isEmpty()) {
            return ResponseEntity.ok().body(ObjectMapperUtils.mapAll(users, UserDto.class));
        }
        return ResponseEntity.noContent().build();
    }

    public ResponseEntity<?> getUser(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            return ResponseEntity.ok().body(ObjectMapperUtils.map(user, UserDto.class));
        }
        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> saveUser(UserDto userDto) throws HttpClientErrorException {
        User user = ObjectMapperUtils.map(userDto, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        try {
            userRepository.save(user);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> updateUser(UserDto userDto) throws HttpClientErrorException {
        Optional<User> user = userRepository.findById(userDto.getUserId());
        if (user.isPresent()) {
            User userToBeSaved = user.get();
            ObjectMapperUtils.map(user, userToBeSaved);
            userToBeSaved.setPassword(passwordEncoder.encode(userDto.getPassword()));
            userRepository.save(user.get());
            return ResponseEntity.noContent().build();
        }
        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> deleteUser(Long userId) throws HttpClientErrorException {
        try {
            userRepository.deleteById(userId);
            return ResponseEntity.noContent().build();
        } catch (EmptyResultDataAccessException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
