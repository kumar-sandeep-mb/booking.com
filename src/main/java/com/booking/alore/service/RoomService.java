package com.booking.alore.service;

import com.booking.alore.entity.Hotel;
import com.booking.alore.entity.Room;
import com.booking.alore.model.RoomDto;
import com.booking.alore.repository.HotelRepository;
import com.booking.alore.repository.RoomRepository;
import com.booking.alore.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

//TODO : handle exceptions
//TODO : put logs
@Service
public class RoomService {
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private HotelRepository hotelRepository;

    public ResponseEntity<?> saveRoom(RoomDto roomDto) {
        Room room = ObjectMapperUtils.map(roomDto, Room.class);
        roomRepository.save(room);
        Hotel hotel = hotelRepository.findById(roomDto.getHotel().getHotelId()).get();
        if (hotel.getMinCost() == 0 || roomDto.getCost() < hotel.getMinCost()) {
            hotel.setMinCost(roomDto.getCost());
            hotelRepository.save(hotel);
        }

        return ResponseEntity.noContent().build();
    }

    public ResponseEntity<?> deleteRoom(Long roomId) {
        try {
            roomRepository.deleteById(roomId);
            return ResponseEntity.noContent().build();
        } catch (EmptyResultDataAccessException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
