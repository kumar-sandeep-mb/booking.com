package com.booking.alore.service;

import com.booking.alore.entity.Hotel;
import com.booking.alore.entity.Review;
import com.booking.alore.model.ReviewDto;
import com.booking.alore.repository.HotelRepository;
import com.booking.alore.repository.ReviewRepository;
import com.booking.alore.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

//TODO : handle exceptions
//TODO : put logs
@Service
public class ReviewService {
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private HotelRepository hotelRepository;

    public ResponseEntity<?> saveReview(ReviewDto reviewDto) {
        Review review = ObjectMapperUtils.map(reviewDto, Review.class);
        try {
            reviewRepository.save(review);
            float averageRatingOfHotel = reviewRepository.findAverageRatingOfHotel(review.getHotel().getHotelId());
            Hotel hotel = hotelRepository.getById(review.getHotel().getHotelId());
            hotel.setRating(averageRatingOfHotel);
            hotelRepository.save(hotel);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> deleteReview(Long reviewId) {
        try {
            reviewRepository.deleteById(reviewId);
            return ResponseEntity.noContent().build();
        } catch (EmptyResultDataAccessException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
