package com.booking.alore.service;

import com.booking.alore.entity.City;
import com.booking.alore.entity.Hotel;
import com.booking.alore.model.HotelDto;
import com.booking.alore.repository.BookingRepository;
import com.booking.alore.repository.CityRepository;
import com.booking.alore.repository.HotelRepository;
import com.booking.alore.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

//TODO : handle exceptions
//TODO : put logs
@Service
public class HotelService {
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private CityRepository cityRepository;
    @Autowired
    private BookingRepository bookingRepository;

    public ResponseEntity<?> getHotels(String cityName, int star, boolean wifiAvailability,
                                       boolean restaurantAvailability, boolean acAvailability, boolean mealsIncluded,
                                       String sortBy) {
        long cityId = 0L;
        if (StringUtils.hasText(cityName)) {
            City city = cityRepository.findByCityName(cityName);
            if (city == null) {
                return ResponseEntity.ok().build();
            }
            cityId = city.getCityId();
        }
        List<Hotel> hotels = hotelRepository.findHotels(cityId, star, wifiAvailability, restaurantAvailability, acAvailability,
                mealsIncluded, sortBy);
        return ResponseEntity.ok(hotels);
    }

    public ResponseEntity<?> getAvailableHotels(Date checkInDate, Date checkOutDate, long roomsRequired) {
        List<Hotel> availableHotels = bookingRepository.findAvailableHotels(checkInDate, checkOutDate, roomsRequired);
        return ResponseEntity.ok(availableHotels);
    }

    public ResponseEntity<?> getHotel(Long hotelId) {
        Optional<Hotel> hotel = hotelRepository.findById(hotelId);
        if (hotel.isPresent()) {
            return ResponseEntity.ok().body(ObjectMapperUtils.map(hotel.get(), HotelDto.class));
        }
        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> saveHotel(HotelDto hotelDto) throws HttpClientErrorException {
        Hotel hotel = ObjectMapperUtils.map(hotelDto, Hotel.class);
        try {
            hotelRepository.save(hotel);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> updateHotel(HotelDto hotelDto) throws HttpClientErrorException {
        Optional<Hotel> hotel = hotelRepository.findById(hotelDto.getHotelId());
        if (hotel.isPresent()) {
            Hotel hotelToBeSaved = hotel.get();
            ObjectMapperUtils.map(hotelDto, hotelToBeSaved);
            hotelRepository.save(hotelToBeSaved);
            return ResponseEntity.noContent().build();
        }
        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> deleteHotel(Long hotelId) throws HttpClientErrorException {
        try {
            hotelRepository.deleteById(hotelId);
            return ResponseEntity.noContent().build();
        } catch (EmptyResultDataAccessException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
